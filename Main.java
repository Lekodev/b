import btree.Arvore;
import btree.Chave;
import java.io.RandomAccessFile;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Arvore arvore = new Arvore(5);
        RandomAccessFile f = new RandomAccessFile("cep.dat", "r");

        System.out.println("Indexando o arquivo.");
        indexar(f, arvore);
        System.out.println("Indexação concluida.");

        Scanner sc = new Scanner(System.in);
        System.out.print(">> ");
        while(sc.hasNext()){
            int indice = Integer.parseInt(sc.nextLine());

            Chave c = arvore.procura(indice);
            if(c != null) {
                f.seek(c.getPos());

                Endereco j = new Endereco();
                j.leEndereco(f);
                System.out.println("Rua: " + j.getLogradouro());
                System.out.println("Estado: " + j.getEstado());
                System.out.println("Cidade: " + j.getCidade());
                System.out.println("Bairro: " + j.getBairro());


                System.out.print(">> ");
            } else {
                System.out.println("Indice não encontrado.");
                System.out.print(">> ");
            }
        }
    }

    public static void indexar(RandomAccessFile f, Arvore arvore) throws IOException{
        long tamanhoArquivo = f.length();
        int tamanhoRegistro = 300;
        long qtdRegistros = tamanhoArquivo/tamanhoRegistro;


        Endereco e = new Endereco();
        for(int i=0; i<qtdRegistros; i++){
            int pos = i*tamanhoRegistro;
            f.seek(pos);
            e.leEndereco(f);
            arvore.adiciona(Integer.parseInt(e.getCep()), pos);
        }
    }

}
