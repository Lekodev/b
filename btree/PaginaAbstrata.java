package btree;

public abstract class PaginaAbstrata {

    public abstract Resposta adiciona(Chave chave);

    public abstract void imprimirIndices();

    public abstract Chave procura(int indice);
}
