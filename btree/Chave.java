package btree;

public class Chave {
    private int indice;
    private int pos;

    public Chave(int indice, int pos){
        this.indice = indice;
        this.pos = pos;
    }

    public int getIndice(){
        return this.indice;
    }

    public int getPos(){
        return this.pos;
    }
}
