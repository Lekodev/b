package btree;

public class Pagina extends PaginaAbstrata {
    private Chave[] chaves;
    private int qtd = 0;

    // Construtor padrão
    public Pagina(int tamanhoPagina){
        this.chaves = new Chave[tamanhoPagina];
    }

    // Construtor do split
    public Pagina(Chave[] chaves, int qtdRegistros){
        this.chaves = chaves;
        this.qtd = qtdRegistros;
    }

    public Resposta adiciona(Chave chave){
        for (int i = 0; i < chaves.length; i++) {
            if (chaves[i] == null) {
                chaves[i] = chave;
                break;
            } else {
                if (chave.getIndice() < chaves[i].getIndice()) {
                    if(chave.getIndice() == chaves[i].getIndice())
                        throw new RuntimeException("Indice duplicado.");

                    insereChave(chave, i);
                    break;
                }
            }
        }
        qtd++;

        if(qtd == chaves.length){
            int indiceSplit = chaves.length /2;
            return new Resposta(this.chaves[indiceSplit].getIndice() ,split(indiceSplit));
        } else
            return new Resposta();
    }

    private PaginaAbstrata split(int indiceSplit){
        Chave[] temp = new Chave[chaves.length];
        int tempQtd = 0;

        for(int i= (indiceSplit+1); i < chaves.length; i++){
            temp[tempQtd] = chaves[i];
            chaves[i] = null;
            tempQtd++;
        }
        qtd -= tempQtd;

        return new Pagina(temp, tempQtd);
    }

    // insere a chave em uma posição e "empurra" os outros registros do array "pra frente"
    private void insereChave(Chave chave, int pos){
        Chave temp;
        Chave nova = chave;

        for(int i = pos; i< chaves.length; i++){
            temp = chaves[i];
            chaves[i] = nova;
            nova = temp;

            if(temp == null)
                break;
        }
    }

    public Chave[] getChaves(){
        return this.chaves;
    }

    public void imprimirIndices(){
        System.out.print("Indices na pagina: ");
        for(Chave c : chaves){
            if(c != null){
                System.out.print(+ c.getIndice() + " ");
            }
        }
        System.out.println("");
    }

    @Override
    public Chave procura(int indice) {
        for(Chave c : chaves){
            if(c != null && c.getIndice() == indice)
                return c;

        }
        return null;
    }
}
