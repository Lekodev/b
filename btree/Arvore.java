package btree;

public class Arvore {

    private PaginaAbstrata raiz;
    private int tamanhoPagina;

    public Arvore(int tamanho){
        this.raiz= new Pagina(tamanho);
        this.tamanhoPagina = tamanho;
    }

    public void adiciona(int indice, int pos){
        if(indice <=0)
            throw new RuntimeException("Indice em range inválida.");

        Chave chave = new Chave(indice, pos);
        Resposta r = raiz.adiciona(chave);
        if(r.isSplit()){
            PaginaIntermediaria novaRaiz = new PaginaIntermediaria(tamanhoPagina, r.getIndice(), raiz, r.getPagina());
            raiz = novaRaiz;
        } else {

        }
    }

    public Chave procura(int indice){
        return raiz.procura(indice);
    }

    public void imprimirIndices(){
        raiz.imprimirIndices();
    }
}
