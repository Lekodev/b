package btree;

public class Resposta {

    private int indice;
    private PaginaAbstrata pagina;
    private boolean split;

    public Resposta(int indice, PaginaAbstrata pagina){
        this.indice=  indice;
        this.pagina = pagina;
        this.split = true;
    }

    public Resposta(){
        this.split = false;
    }

    public int getIndice(){
        return this.indice;
    }

    public PaginaAbstrata getPagina(){
        return this.pagina;
    }

    public boolean isSplit(){
        return this.split;
    }
}
