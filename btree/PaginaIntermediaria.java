package btree;

class PaginaIntermediaria extends PaginaAbstrata {
    private int[] indices;
    private PaginaAbstrata ponteiros[];
    private int qtd;

    public PaginaIntermediaria(int tamanho, int primIndice, PaginaAbstrata p1, PaginaAbstrata p2){
        indices = new int[tamanho];
        ponteiros = new PaginaAbstrata[tamanho+1];
        indices[0] = primIndice;
        ponteiros[0] = p1;
        ponteiros[1] = p2;
        qtd++;
    }

    public PaginaIntermediaria(int qtd, int[] indices, PaginaAbstrata[] ponteiros){
        this.qtd = qtd;
        this.indices = indices;
        this.ponteiros = ponteiros;
    }

    @Override
    public Resposta adiciona(Chave chave) {
        Resposta r = null;
        for(int i =0; i< indices.length; i++){
            if(chave.getIndice() <= indices[i]){
                r = ponteiros[i].adiciona(chave);
                break;
            }
        }

        if(r == null){
            int ultimaPosPonteiro = qtd;
            r = ponteiros[ultimaPosPonteiro].adiciona(chave);
        }

        if(r.isSplit()) {
            for (int i = 0; i < indices.length; i++) {

                if(indices[i] == 0){
                    salva(i, r.getIndice(), r.getPagina());
                    qtd++;
                    break;
                }
                if (r.getIndice() < indices[i]) {
                    salva(i, r.getIndice(), r.getPagina());
                    qtd++;
                    break;
                }
            }

            if (qtd == indices.length) {
                int indiceSplit = indices.length / 2;
                return new Resposta(this.indices[indiceSplit], split(indiceSplit));
//                return new btree.Resposta();
            } else {
                return new Resposta();
            }
        } else{
            return new Resposta();
        }
    }

    private void salva(int pos, int indice, PaginaAbstrata pag){
        if(pos == indices.length){

        } else {
            int tempI = indices[pos];
            PaginaAbstrata tempPag = ponteiros[pos+1];
            indices[pos] = indice;
            ponteiros[pos+1] = pag;

            salva(pos + 1, tempI, tempPag);
        }
    }

    private PaginaAbstrata split(int pos){
        int[] tempIndices = new int[indices.length];
        PaginaAbstrata[] tempPaginas = new PaginaAbstrata[indices.length+1];
        int tempQtd = 0;

        indices[pos] = 0;
        qtd--;
        for(int i= (pos+1); i < indices.length; i++){
            tempIndices[tempQtd] = indices[i];
            tempPaginas[tempQtd] = ponteiros[i];
            indices[i] = 0;
            ponteiros[i] = null;
            tempQtd++;
        }
        tempPaginas[tempQtd] = ponteiros[ponteiros.length -1];
        ponteiros[ponteiros.length -1] = null;

        qtd -= tempQtd;

        return new PaginaIntermediaria(tempQtd, tempIndices, tempPaginas);
    }

    @Override
    public void imprimirIndices() {
        System.out.print("Indices na pagina intermediaria: ");
        for(int i : indices){
            System.out.print(" " + i + " ");
        }
        System.out.println("");
        for(PaginaAbstrata p : ponteiros){
            if(p != null)
                p.imprimirIndices();
        }
    }

    @Override
    public Chave procura(int indice) {
        for(int i = 0; i < qtd; i++){
            if(indice <= indices[i])
                return ponteiros[i].procura(indice);
        }

        return ponteiros[qtd].procura(indice);
    }
}